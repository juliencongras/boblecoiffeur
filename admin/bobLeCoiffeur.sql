drop database if exists bobCoiffeur;
create database bobCoiffeur;
use bobCoiffeur;

create table miscInfos(
    id int auto_increment primary key,
    adresse varchar(255),
    telephone varchar(255),
    horaires varchar(255)
);

create table identifiantsAdmin(
    id int auto_increment primary key,
    identifiant varchar(255),
    mdp varchar(255)
);

create table infosClients(
    id int auto_increment primary key,
    dateRDV varchar(255),
    heure varchar(255),
    tel varchar(255),
    nom varchar(255)
);

create table horairesDisponibles(
    heure varchar(255)
);

INSERT INTO miscInfos (adresse, telephone, horaires) VALUES 
(
 '5 rue du coiffeur, 11000 Carcassonne','01.01.01.01.01.', 
 'Ouvert de 9h à 12h sauf le Dimanche.'
);

INSERT INTO identifiantsAdmin (identifiant, mdp) VALUES 
(
 'BobLeCoiffeur', 
 'JZbfcoqei65ZDbj0223ZBF3M05d'
);

INSERT INTO horairesDisponibles (heure) VALUES 
('9h'), ('9h30'), ('10h'), ('10h30'), ('11h'), ('11h30'), ('12h');

drop user if exists bob@'127.0.0.1';
create user bob@'127.0.0.1' identified by 'coiffeur';
grant all privileges on bobCoiffeur.* to bob@'127.0.0.1';