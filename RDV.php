<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste des rendez vous</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php 
    include_once("header.php");
    ?>
    <form action="RDV.php" method="post">
        <p>Choisir une date : 
        <input type="date" name="dateChoose" id="dateChoose" value="<?php echo date("Y-m-d"); ?>">
        <button>OK</button></p>
    </form>
    <?php
        $date = $_POST['dateChoose'];
        if (isset($date)){
            require('pdo.php');
            global $pdo;
            $req = $pdo->prepare('select * from infosClients where dateRDV = ?;');
            $req->execute([$date]);
            $infosClients = $req->fetchAll();
        }
    ?>
    <table id="infosClientsContainer">
        <thead>
            <tr>
                <th colspan="1">Heure</th>
                <th colspan="1">Téléphone</th>
                <th colspan="1">Nom</th>
            </tr>
        </thead>
        <tbody>

        <?php foreach($infosClients as $info){ ?>
            <tr>
                <td><?php echo $info['heure'] ?></td>
                <td><a href="tel:<?php echo $info['tel'] ?>"><?php echo $info['tel'] ?></a></td>
                <td><?php echo $info['nom'] ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    
    <?php 
    include_once("footer.php")
    ?>
</body>
</html>