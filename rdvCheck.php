<?php

    include_once("pdo.php");

    if( isset($_POST['date']) 
    && isset($_POST['horaires']) 
    && isset($_POST['clientTel'])
    && isset($_POST['clientNom'])

    ){         
        $date = $_POST['date'];
        $horaires = $_POST['horaires'];
        $tel = $_POST['clientTel'];
        $nom = $_POST['clientNom'];

        include_once('pdo.php');

        $reqTwo = $pdo->prepare('select * from infosClients where dateRDV = ? AND heure = ?;');
        $reqTwo->execute([$date, $horaires]);
        $dateCheck = $reqTwo->fetchAll();
        if(!$dateCheck){

            $req = $pdo->prepare('INSERT INTO infosClients (dateRDV, heure, tel ,nom) VALUES (?,?,?,?);');
            $req->execute([$date, $horaires, $tel, $nom]);

            header('location: index.php');
        }
        else{ ?>
        <p>Rendez vous non disponible. <a href="index.php">Retour au site.</a></p>
        <?php }
    }

?>