<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fancy Bob</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
        require('pdo.php');
        $req = $pdo->query('select * from miscInfos;');
        $miscInfos = $req->fetchAll();

        $reqTwo = $pdo->query('select * from horairesDisponibles;');
        $horaireDispo = $reqTwo->fetchAll();
    ?>
    <?php
        include_once("header.php")
    ?>
    <div id="main">
        <img id="imgBob" src="admin/ressources/images/fancy_bob.png" alt="Bob le coiffeur.">
    </div>
    <div id="miscInfo">
        <div id="miscInfoTextContainer">
            <p><?= $miscInfos[0]['adresse'] ?></p>
            <p>Téléphone : <a href="tel:<?= $miscInfos[0]['telephone']  ?>">
            <?= $miscInfos[0]['telephone'] ?></a></p>
            <p><?= $miscInfos[0]['horaires']  ?></p>
            <?php 
            if($_SESSION["admin"] == true){ ?>
                <a href="admin/changeMiscInfos.php">Modifier</a>
            <?php }
            ?>
        </div>
    </div>
    <form action="rdvCheck.php" method="post">
        <input onclick="dateCheck()" type="date" name="date" id="" min="<?php echo date("Y-m-d"); ?>" value="<?php echo date("Y-m-d"); ?>">
        <fieldset>
        <legend>Choisir un horaire :</legend>
            <div>
                <?php foreach($horaireDispo as $horaire){ ?>
                    <input type="radio" id="<?php echo $horaire['heure'] ?>" name="horaires" value="<?php echo $horaire['heure'] ?>">
                    <label for="<?php echo $horaire['heure'] ?>"><?php echo $horaire['heure'] ?></label>
                <?php } ?>
            </div>
        </fieldset>
        <p>Téléphone : <input type="tel" name="clientTel" id=""></p>
        <p>Nom : <input type="text" name="clientNom"></p>
        <button>OK</button>
    </form>
    <?php 
        include_once "footer.php"
    ?>
<script src="admin/script.js"></script>
</body>
</html>